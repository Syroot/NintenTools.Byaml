﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Syroot.BinaryData;
using Syroot.Maths;

namespace Syroot.NintenTools.Byaml
{
    internal static class XmlConverter
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private static readonly XNamespace _yamlconvNs = "yamlconv";

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal static ByamlFormat GetFormat(XDocument xDocument)
        {
            XElement root = xDocument.Root;
            Endian endian = root.Attribute(_yamlconvNs + "endianness").Value.ToString() == "little" ? Endian.Little : Endian.Big;
            bool supportPaths = root.Attribute(_yamlconvNs + "offsetCount").Value.ToString() == "4";
            ushort version = UInt16.Parse(root.Attribute(_yamlconvNs + "byamlVersion").Value.ToString());
            return new ByamlFormat { Endian = endian, SupportPaths = supportPaths, Version = version };
        }

        internal static dynamic FromXDocument(XDocument xDocument)
        {
            if (xDocument == null)
                throw new ArgumentNullException(nameof(xDocument));
            if (xDocument.Root?.Name != "yaml")
                throw new ArgumentException("Incompatible XML data. A \"yaml\" root element is required.");

            // Retrieve information from root node.
            ByamlFormat format;
            switch (xDocument.Root.Attribute(_yamlconvNs + "endianness")?.Value)
            {
                case "big": format.Endian = Endian.Big; break;
                case "little": format.Endian = Endian.Little; break;
                default: format.Endian = Endian.System; break;
            }
            format.Version = UInt16.TryParse(xDocument.Root.Attribute(_yamlconvNs + "version")?.Value,
                out ushort version) ? version : (ushort)1;

            // Generate the root node.
            return ReadNode(xDocument.Root);
        }

        internal static XDocument ToXDocument(dynamic root, ByamlFormat format)
        {
            if (!(root is IEnumerable))
                throw new InvalidOperationException("BYAML root must be an IEnumerable.");

            // Generate the root element.
            XElement rootNode = SaveNode("yaml", root, false);

            // Insert namespace declarations at front of root element.
            List<XAttribute> attribs = rootNode.Attributes().ToList();
            attribs.Insert(0, new XAttribute(XNamespace.Xmlns + "yamlconv", "yamlconv"));
            attribs.Insert(1, new XAttribute(_yamlconvNs + "endianness",
                ByteConverter.GetConverter(format.Endian).Endian == Endian.Little ? "little" : "big"));
            attribs.Insert(2, new XAttribute(_yamlconvNs + "offsetCount", format.SupportPaths ? 4 : 3));
            attribs.Insert(3, new XAttribute(_yamlconvNs + "byamlVersion", format.Version));
            rootNode.Attributes().Remove();
            rootNode.Add(attribs);

            // Create and return the XDocument with the correct header.
            return new XDocument(new XDeclaration("1.0", "utf-8", null), rootNode);
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static dynamic ReadNode(XElement node)
        {
            dynamic convertValue(string value)
            {
                if (value == "true")
                    return true;
                else if (value == "false")
                    return false;
                else if (value.EndsWith("f"))
                    return Single.Parse(value.Substring(0, value.Length - "f".Length), CultureInfo.InvariantCulture);
                else if (value.EndsWith("u"))
                    return UInt32.Parse(value.Substring(0, value.Length - "u".Length), CultureInfo.InvariantCulture);
                else if (value.EndsWith("i64"))
                    return Int64.Parse(value.Substring(0, value.Length - "i64".Length), CultureInfo.InvariantCulture);
                else if (value.EndsWith("u64"))
                    return UInt64.Parse(value.Substring(0, value.Length - "u64".Length), CultureInfo.InvariantCulture);
                else if (value.EndsWith("d"))
                    return Double.Parse(value.Substring(0, value.Length - "d".Length), CultureInfo.InvariantCulture);
                else
                    return Int32.Parse(value, CultureInfo.InvariantCulture);
            }

            string convertString() => node.Value.ToString();

            List<ByamlPathPoint> convertPath()
            {
                List<ByamlPathPoint> path = new List<ByamlPathPoint>();
                foreach (XElement pathPoint in node.Elements("point"))
                    path.Add(convertPathPoint(pathPoint));
                return path;
            }

            ByamlPathPoint convertPathPoint(XElement pathPoint)
            {
                return new ByamlPathPoint
                {
                    Position = new Vector3F(
                        convertValue(pathPoint.Attribute("x")?.Value ?? "0f"),
                        convertValue(pathPoint.Attribute("y")?.Value ?? "0f"),
                        convertValue(pathPoint.Attribute("z")?.Value ?? "0f")),
                    Normal = new Vector3F(
                        convertValue(pathPoint.Attribute("nx")?.Value ?? "0f"),
                        convertValue(pathPoint.Attribute("ny")?.Value ?? "0f"),
                        convertValue(pathPoint.Attribute("nz")?.Value ?? "0f")),
                    Unknown = convertValue(pathPoint.Attribute("val")?.Value ?? "0")
                };
            }

            Dictionary<string, dynamic> convertDictionary()
            {
                Dictionary<string, dynamic> dictionary = new Dictionary<string, dynamic>();
                foreach (XElement element in node.Elements())
                    dictionary.Add(XmlConvert.DecodeName(element.Name.ToString()), ReadNode(element));
                // Only keep non-namespaced attributes for now to filter out yamlconv and xml(ns) ones.
                foreach (XAttribute attribute in node.Attributes().Where(x => x.Name.Namespace == XNamespace.None))
                    dictionary.Add(XmlConvert.DecodeName(attribute.Name.ToString()), convertValue(attribute.Value));
                return dictionary;
            }

            List<dynamic> convertArray()
            {
                List<dynamic> array = new List<dynamic>();
                foreach (XElement element in node.Elements("value"))
                    array.Add(ReadNode(element));
                return array;
            }


            // Detecting the special "type" attribute like this is unsafe as it could also be a dictionary with a "type"
            // key. Yamlconv should have namespaced its attribute to safely identify it.
            switch (node.Attributes("type").SingleOrDefault()?.Value)
            {
                // TODO: Add null support. Can null be set for value types?
                // TODO: Add reference support. Use Element with encoded XPath.
                case null when node.HasAttributes || node.HasElements: return convertDictionary();
                case null: return convertValue(node.Value);
                case "array": return convertArray();
                case "path": return convertPath();
                case "string": return convertString();
                default: throw new ByamlException("Unknown XML contents.");
            }
        }

        private static XObject SaveNode(string name, dynamic node, bool isArrayElement)
        {
            XObject convertValue(object value)
                => isArrayElement ? new XElement(name, value) : (XObject)new XAttribute(name, value);

            XElement convertString(string stringNode)
                => new XElement(name, new XAttribute("type", "string"), stringNode);

            XElement convertPathPoint(ByamlPathPoint pathPointNode)
            {
                return new XElement("point",
                    new XAttribute("x", getSingleString(pathPointNode.Position.X)),
                    new XAttribute("y", getSingleString(pathPointNode.Position.Y)),
                    new XAttribute("z", getSingleString(pathPointNode.Position.Z)),
                    new XAttribute("nx", getSingleString(pathPointNode.Normal.X)),
                    new XAttribute("ny", getSingleString(pathPointNode.Normal.Y)),
                    new XAttribute("nz", getSingleString(pathPointNode.Normal.Z)),
                    new XAttribute("val", getInt32String(pathPointNode.Unknown)));
            }

            XObject convertPath(List<ByamlPathPoint> pathNode)
            {
                XElement xElement = new XElement(name, new XAttribute("type", "path"));
                foreach (dynamic element in pathNode)
                    xElement.Add(SaveNode("point", element, true));
                return xElement;
            }

            XElement convertDictionary(IDictionary<string, dynamic> dictionaryNode)
            {
                XElement xElement = new XElement(name);
                foreach (KeyValuePair<string, dynamic> element in dictionaryNode.OrderBy(x => x.Key, StringComparer.Ordinal))
                    xElement.Add(SaveNode(element.Key, element.Value, false));
                return xElement;
            }

            XElement convertArray(IEnumerable arrayNode)
            {
                XElement xElement = new XElement(name, new XAttribute("type", "array"));
                foreach (dynamic element in arrayNode)
                    xElement.Add(SaveNode("value", element, true));
                return xElement;
            }

            string getBooleanString(Boolean value) => value ? "true" : "false";
            string getInt32String(Int32 value) => value.ToString(CultureInfo.InvariantCulture);
            string getSingleString(Single value) => value.ToString(CultureInfo.InvariantCulture) + "f";
            string getUInt32String(UInt32 value) => value.ToString(CultureInfo.InvariantCulture) + "u";
            string getInt64String(Int64 value) => value.ToString(CultureInfo.InvariantCulture) + "i64";
            string getUInt64String(UInt64 value) => value.ToString(CultureInfo.InvariantCulture) + "u64";
            string getDoubleString(Double value) => value.ToString(CultureInfo.InvariantCulture) + "d";


            name = XmlConvert.EncodeName(name);
            switch (node)
            {
                // TODO: Add null support. Can null be set for value types?
                // TODO: Add reference support. Use Element with encoded XPath.
                case Boolean booleanNode: return convertValue(getBooleanString(booleanNode));
                case Int32 int32Node: return convertValue(getInt32String(int32Node));
                case Single singleNode: return convertValue(getSingleString(singleNode));
                case UInt32 uint32Node: return convertValue(getUInt32String(uint32Node));
                case Int64 int64Node: return convertValue(getInt64String(int64Node));
                case UInt64 uint64Node: return convertValue(getUInt64String(uint64Node));
                case Double doubleNode: return convertValue(getDoubleString(doubleNode));
                case String stringNode: return convertString(stringNode);
                case ByamlPathPoint pathPointNode: return convertPathPoint(pathPointNode);
                case List<ByamlPathPoint> pathNode: return convertPath(pathNode);
                case IDictionary<string, dynamic> dictionaryNode: return convertDictionary(dictionaryNode);
                case IEnumerable arrayNode: return convertArray(arrayNode);
                default: throw new ByamlException($"Cannot convert type '{node.GetType()}' to XML.");
            }
        }
    }
}
