﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Syroot.BinaryData;

namespace Syroot.NintenTools.Byaml
{
    internal class DynamicSaver
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private BinaryStream _stream;
        private Dictionary<object, uint> _nodeCache;
        private List<string> _nameArray;
        private List<string> _stringArray;
        private List<List<ByamlPathPoint>> _pathArray;

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal void Save(Stream stream, IEnumerable root, ByamlFormat format)
        {
            if (format.Version > ByamlData.MaxSupportedVersion)
                throw new ArgumentException("Unsupported BYAML version.", nameof(format));

            _nodeCache = new Dictionary<object, uint>();

            // Generate the name, string and path array nodes.
            _nameArray = new List<string>();
            _stringArray = new List<string>();
            _pathArray = new List<List<ByamlPathPoint>>();
            CollectNodeContents(root);
            _nameArray.Sort(StringComparer.Ordinal);
            _stringArray.Sort(StringComparer.Ordinal);

            // Create a configured BinaryStream on the given stream.
            using (_stream = new BinaryStream(stream, ByteConverter.GetConverter(format.Endian),
                ByamlData.ShiftJisEncoding, BooleanCoding.Dword, stringCoding: StringCoding.ZeroTerminated,
                leaveOpen: true))
            {
                // Write magic bytes and version.
                _stream.WriteUInt16(0x4259); // "BY"
                _stream.WriteUInt16(format.Version);

                // Write the main node offsets.
                uint nameArrayOffset = _stream.ReserveOffset();
                uint stringArrayOffset = _stream.ReserveOffset();
                uint pathArrayOffset = format.SupportPaths ? _stream.ReserveOffset() : 0;
                uint rootOffset = _stream.ReserveOffset();

                // Write the main nodes.
                if (_nameArray.Count > 0)
                    WriteEnumerableNode(_nameArray, NodeType.StringArray, nameArrayOffset);
                if (_stringArray.Count > 0)
                    WriteEnumerableNode(_stringArray, NodeType.StringArray, stringArrayOffset);
                if (format.SupportPaths && _pathArray.Count > 0)
                    WriteEnumerableNode(_pathArray, NodeType.PathArray, pathArrayOffset);
                WriteEnumerableNode(root, GetNodeType(root), rootOffset);
            }
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static NodeType GetNodeType(object node)
        {
            switch (node)
            {
                case String _: return NodeType.StringIndex;
                case List<ByamlPathPoint> _: return NodeType.PathIndex;
                case IDictionary<string, object> _: return NodeType.Dictionary;
                case IEnumerable _: return NodeType.Array;
                case Boolean _: return NodeType.Boolean;
                case Int32 _: return NodeType.Int32;
                case Single _: return NodeType.Single;
                case UInt32 _: return NodeType.UInt32;
                case Int64 _: return NodeType.Int64;
                case UInt64 _: return NodeType.UInt64;
                case Double _: return NodeType.Double;
                case null: return NodeType.Null;
                default: throw new ByamlException($"Type '{node.GetType()}' is not supported as a BYAML node.");
            }
        }

        private void CollectNodeContents(object node)
        {
            switch (node)
            {
                case string stringNode:
                    if (!_stringArray.Contains(stringNode))
                        _stringArray.Add(stringNode);
                    break;
                case List<ByamlPathPoint> pathNode:
                    _pathArray.Add(pathNode);
                    break;
                case IDictionary<string, object> dictionaryNode:
                    foreach (KeyValuePair<string, object> entry in dictionaryNode)
                    {
                        if (!_nameArray.Contains(entry.Key))
                            _nameArray.Add(entry.Key);
                        CollectNodeContents(entry.Value);
                    }
                    break;
                case IEnumerable arrayNode:
                    foreach (object childNode in arrayNode)
                        CollectNodeContents(childNode);
                    break;
            }
        }

        private uint WriteNodeValue(object node)
        {
            switch (node)
            {
                case String stringNode: _stream.WriteInt32(_stringArray.IndexOf(stringNode)); return 0;
                case List<ByamlPathPoint> pathNode: _stream.WriteInt32(_pathArray.IndexOf(pathNode)); return 0;
                case IDictionary<string, object> _:
                case IEnumerable _: // offset
                    uint offset = (uint)_stream.Position;
                    _stream.WriteInt32(0);
                    return offset;
                case Boolean booleanNode: _stream.WriteBoolean(booleanNode); return 0;
                case Int32 int32Node: _stream.WriteInt32(int32Node); return 0;
                case Single singleNode: _stream.WriteSingle(singleNode); return 0;
                case UInt32 uint32Node: _stream.WriteUInt32(uint32Node); return 0;
                case Int64 int64Node: _stream.WriteInt64(int64Node); return 0;
                case UInt64 uint64Node: _stream.WriteUInt64(uint64Node); return 0;
                case Double doubleNode: _stream.WriteDouble(doubleNode); return 0;
                case null: _stream.WriteInt32(0); return 0;
                default: throw new ByamlException($"Type {node.GetType()} unsupported as BYAML data.");
            }
        }

        private void WriteEnumerableNode(IEnumerable node, NodeType type, uint offset)
        {
            // Check if the node was already written and link to it, otherwise write it out.
            if (_nodeCache.TryGetValue(node, out uint position))
            {
                _stream.SatisfyOffset(offset, position);
            }
            else
            {
                position = (uint)_stream.Position;
                _stream.SatisfyOffset(offset, position);
                _nodeCache.Add(node, position);

                int count = 0;
                foreach (object item in node)
                    count++;

                // Write the enumerable node header and the contents.
                _stream.WriteByte((byte)type);
                _stream.Write3ByteInt32(count);
                switch (type)
                {
                    case NodeType.Array: WriteArray(node, count); break;
                    case NodeType.Dictionary: WriteDictionary((IDictionary<string, object>)node, count); break;
                    case NodeType.StringArray: WriteStringArray((List<string>)node); break;
                    case NodeType.PathArray: WritePathArray((List<List<ByamlPathPoint>>)node); break;
                }
            }
        }

        private void WriteArray(IEnumerable node, int count)
        {
            // Write the element types.
            foreach (object element in node)
                _stream.WriteByte((byte)GetNodeType(element));
            _stream.Align(4);

            // Write the elements.
            List<IEnumerable> enumerables = new List<IEnumerable>();
            List<uint> offsets = new List<uint>();
            foreach (object child in node)
            {
                uint offset = WriteNodeValue(child);
                if (offset > 0)
                {
                    offsets.Add(offset);
                    enumerables.Add((IEnumerable)child);
                }
            }

            // Write offset enumerable nodes.
            for (int i = 0; i < enumerables.Count; i++)
            {
                IEnumerable enumerable = enumerables[i];
                WriteEnumerableNode(enumerable, GetNodeType(enumerable), offsets[i]);
            }
        }

        private void WriteDictionary(IDictionary<string, object> node, int count)
        {
            // Remember enumerable elements to write them in original order and satisfy offsets to them later.
            List<EnumerableNode> enumerables = node
                .Where(x => GetNodeType(x.Value).IsEnumerable())
                .Select(x => new EnumerableNode { Node = (IEnumerable)x.Value })
                .ToList();

            // Write the key-value pairs, sorted ordinally by key.
            foreach (KeyValuePair<string, object> element in node.OrderBy(x => x.Key, StringComparer.Ordinal))
            {
                // Write index of the key string in the name array and the type.
                _stream.Write3ByteInt32(_nameArray.IndexOf(element.Key));
                _stream.WriteByte((byte)GetNodeType(element.Value));

                // Write the elements. Remember offsets to enumerable nodes.
                uint offset = WriteNodeValue(element.Value);
                if (offset > 0)
                    enumerables.Where(x => x.Node == element.Value && x.Offset == 0).First().Offset = offset;
            }

            // Write enumerable nodes and satisfy one or multiple offsets to each.
            foreach (EnumerableNode enumerable in enumerables)
                WriteEnumerableNode(enumerable.Node, GetNodeType(enumerable.Node), enumerable.Offset);
        }

        private void WriteStringArray(List<string> node)
        {
            // Write the offsets to the strings, where the last one points to the end of the last string.
            long offset = sizeof(uint) + sizeof(uint) * (node.Count + 1); // Relative to node start + all uint32 offsets.
            foreach (string str in node)
            {
                _stream.WriteUInt32((uint)offset);
                offset += ByamlData.ShiftJisEncoding.GetByteCount(str) + 1;
            }
            _stream.WriteUInt32((uint)offset);

            // Write the 0-terminated strings.
            foreach (string str in node)
                _stream.WriteString(str);
            _stream.Align(4);
        }

        private void WritePathArray(List<List<ByamlPathPoint>> node)
        {
            // Write the offsets to the paths, where the last one points to the end of the last path.
            long offset = sizeof(uint) + sizeof(uint) * (node.Count + 1); // Relative to node start + all uint32 offsets.
            foreach (List<ByamlPathPoint> path in node)
            {
                _stream.WriteUInt32((uint)offset);
                offset += path.Count * ByamlPathPoint.SizeInBytes;
            }
            _stream.WriteUInt32((uint)offset);

            // Write the paths.
            foreach (List<ByamlPathPoint> path in node)
                _stream.WritePath(path);
        }

        // ---- CLASSES, STRUCTS & ENUMS -------------------------------------------------------------------------------

        private class EnumerableNode
        {
            internal IEnumerable Node;
            internal uint Offset;
        }
    }
}
