﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Syroot.BinaryData;
using Syroot.NintenTools.Byaml.Collections;

namespace Syroot.NintenTools.Byaml
{
    internal class DynamicLoader
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private BinaryStream _stream;
        private Dictionary<uint, object> _nodeCache;
        private List<string> _nameArray;
        private List<string> _stringArray;
        private List<List<ByamlPathPoint>> _pathArray;

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal static ByamlFormat GetFormat(Stream stream)
        {
            ByamlFormat format = new ByamlFormat();
            using (stream.TemporarySeek())
            {
                // Detect endianness via magic bytes.
                byte[] magic = stream.ReadBytes(sizeof(ushort));
                if (magic[0] == 'Y' && magic[1] == 'B')
                    format.Endian = Endian.Little;
                else if (magic[0] == 'B' && magic[1] == 'Y')
                    format.Endian = Endian.Big;
                else
                    throw new ByamlException("Invalid BYAML header.");
                using (BinaryStream binaryStream = new BinaryStream(stream, ByteConverter.GetConverter(format.Endian),
                    leaveOpen: true))
                {
                    // Read the version.
                    format.Version = binaryStream.ReadUInt16();

                    // Paths are supported if the third offset is a path array (or null) and the fourth a root.
                    stream.Seek(2 * sizeof(uint)); // Skip name and string array.
                    NodeType thirdNodeType = PeekNodeType(binaryStream);
                    stream.Seek(sizeof(uint));
                    NodeType fourthNodeType = PeekNodeType(binaryStream);
                    format.SupportPaths = (thirdNodeType == NodeType.None || thirdNodeType == NodeType.PathArray)
                        && (fourthNodeType == NodeType.Array || fourthNodeType == NodeType.Dictionary);
                }
            }
            return format;
        }

        internal dynamic Load(Stream stream)
        {
            _nodeCache = new Dictionary<uint, object>();
            ByamlFormat format = GetFormat(stream);

            using (_stream = new BinaryStream(stream, ByteConverter.GetConverter(format.Endian),
                ByamlData.ShiftJisEncoding, BooleanCoding.Dword, stringCoding: StringCoding.ZeroTerminated,
                leaveOpen: true))
            {
                // Skip the already parsed magic and version.
                stream.Position = sizeof(ushort) * 2;

                // Read the root nodes.
                _nameArray = ReadEnumerableNode(_stream.ReadUInt32());
                _stringArray = ReadEnumerableNode(_stream.ReadUInt32());
                if (format.SupportPaths)
                    _pathArray = ReadEnumerableNode(_stream.ReadUInt32());
                return ReadEnumerableNode(_stream.ReadUInt32());
            }
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static NodeType PeekNodeType(BinaryStream stream)
        {
            using (stream.TemporarySeek())
            {
                // If the offset is invalid, the type cannot be determined.
                uint offset = stream.ReadUInt32();
                if (offset > 0 && offset < stream.Length)
                {
                    // Seek to the offset and try to read a valid type.
                    stream.Position = offset;
                    NodeType nodeType = (NodeType)stream.ReadByte();
                    if (Enum.IsDefined(typeof(NodeType), nodeType))
                        return nodeType;
                }
            }
            return NodeType.None;
        }

        private dynamic ReadNodeValue(NodeType type)
        {
            switch (type)
            {
                case NodeType.Array:
                case NodeType.Dictionary:
                case NodeType.StringArray:
                case NodeType.PathArray:
                    return _stream.ReadUInt32(); // offset
                case NodeType.StringIndex: return _stringArray[_stream.ReadInt32()];
                case NodeType.PathIndex: return _pathArray[_stream.ReadInt32()];
                case NodeType.Boolean: return _stream.ReadBoolean();
                case NodeType.Int32: return _stream.ReadInt32();
                case NodeType.Single: return _stream.ReadSingle();
                case NodeType.UInt32: return _stream.ReadUInt32();
                case NodeType.Int64: return _stream.ReadInt64();
                case NodeType.UInt64: return _stream.ReadUInt64();
                case NodeType.Double: return _stream.ReadDouble();
                case NodeType.Null: _stream.Seek(sizeof(int)); return null;
                default: throw new ByamlException($"Invalid node type {type}.");
            }
        }

        private dynamic ReadEnumerableNode(uint offset)
        {
            // Enumerable nodes are stored at offsets. If they were already read (judging from their unique offset),
            // return their instance, otherwise read and cache them.
            object node = null;
            if (offset > 0 && !_nodeCache.TryGetValue(offset, out node))
            {
                using (_stream.TemporarySeek(offset, SeekOrigin.Begin))
                {
                    NodeType type = (NodeType)_stream.ReadByte();
                    int length = _stream.Read3ByteInt32();
                    switch (type)
                    {
                        case NodeType.Array: node = ReadArray(length); break;
                        case NodeType.Dictionary: node = ReadDictionary(length); break;
                        case NodeType.StringArray: node = ReadStringArray(length); break;
                        case NodeType.PathArray: node = ReadPathArray(length); break;
                        default: throw new ByamlException($"Invalid enumerable node type {type}.");
                    }
                    _nodeCache.Add(offset, node);
                }
            }
            return node;
        }

        private List<dynamic> ReadArray(int length)
        {
            List<dynamic> array = new List<dynamic>(length);

            // Read the element types.
            IEnumerable<NodeType> types = _stream.ReadBytes(length).Select(x => (NodeType)x);
            _stream.Align(4);

            // Read the elements.
            foreach (NodeType type in types)
            {
                dynamic value = ReadNodeValue(type);
                if (type.IsEnumerable())
                    array.Add(ReadEnumerableNode(value));
                else
                    array.Add(value);
            }

            return array;
        }

        private OrderedDictionary<string, dynamic> ReadDictionary(int length)
        {
            // Keep the order of enumerable nodes as in the original file.
            OrderedDictionary<string, dynamic> dictionary = new OrderedDictionary<string, dynamic>(length);
            SortedList<uint, string> enumerables = new SortedList<uint, string>(new DuplicateKeyComparer<uint>());

            // Read the elements.
            while (length-- > 0)
            {
                string name = _nameArray[_stream.Read3ByteInt32()];
                NodeType type = (NodeType)_stream.ReadByte();
                dynamic value = ReadNodeValue(type);
                // Add primitive values directly, queue offset enumerable nodes to read them afterwards.
                if (type.IsEnumerable())
                    enumerables.Add(value, name);
                else
                    dictionary.Add(name, value);
            }

            // Read the offset enumerable nodes in the order of how they appear in the file.
            foreach (KeyValuePair<uint, string> enumerable in enumerables)
                dictionary.Add(enumerable.Value, ReadEnumerableNode(enumerable.Key));

            return dictionary;
        }

        private List<string> ReadStringArray(int length)
        {
            long nodeOffset = _stream.Position - sizeof(uint); // Element offsets are relative to the start of node.
            List<string> stringArray = new List<string>(length);

            // Read the element offsets.
            uint[] offsets = _stream.ReadUInt32s(length);

            // Read the elements.
            foreach (uint offset in offsets)
            {
                _stream.Position = nodeOffset + offset;
                stringArray.Add(_stream.ReadString());
            }

            return stringArray;
        }

        private List<List<ByamlPathPoint>> ReadPathArray(int length)
        {
            long nodeOffset = _stream.Position - sizeof(uint); // Element offsets are relative to the start of node.
            List<List<ByamlPathPoint>> pathArray = new List<List<ByamlPathPoint>>(length);

            // Read the element offsets.
            uint[] offsets = _stream.ReadUInt32s(length + 1);

            // Read the elements.
            for (int i = 0; i < length; i++)
            {
                _stream.Position = nodeOffset + offsets[i];
                int pointCount = (int)((offsets[i + 1] - offsets[i]) / ByamlPathPoint.SizeInBytes);
                pathArray.Add(_stream.ReadPath(pointCount));
            }

            return pathArray;
        }
    }
}
