﻿namespace Syroot.NintenTools.Byaml
{
    internal enum NodeType : byte
    {
        None,

        StringIndex = 0xA0,
        PathIndex = 0xA1,

        Array = 0xC0,
        Dictionary = 0xC1,
        StringArray = 0xC2,
        PathArray = 0xC3,

        Boolean = 0xD0,
        Int32 = 0xD1,
        Single = 0xD2,
        UInt32 = 0xD3,
        Int64 = 0xD4,
        UInt64 = 0xD5,
        Double = 0xD6,

        Null = 0xFF
    }

    internal static class NodeTypeExtensions
    {
        internal static bool IsEnumerable(this NodeType self) => self >= NodeType.Array && self <= NodeType.PathArray;
    }
}
